public class Application{
	public static void main (String[] args){
	
	Eagle john = new Eagle();
	john.name = "John";
	john.speed = 160;
	john.color = "black";
	john.type = "Bald Eagle";
	
	Eagle bob = new Eagle();
	bob.name = "Bob";
	bob.speed = 320;
	bob.color = "Brown";
	bob.type = "Golden Eagle";
	
	bob.dive(bob.speed);
	john.eat(john.name);
	
	
	Eagle[] aerie = new Eagle[3];
	aerie[0] = john;
	aerie[1] = bob;
	
	System.out.println(aerie[0].name);
	
	aerie[2] = new Eagle();
	aerie[2].name = "Peter";
	aerie[2].speed = 190;
	aerie[2].color = "red";
	aerie[2].type = "red-tailed Hawk";
	
	
	}
}


/* System.out.println */