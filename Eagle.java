public class Eagle{
	public String name;
	public int speed;
	public String color;
	public String type;
	
	public void dive(int speed){
		if (speed > 100){
			System.out.println("I just dived at 120 km's per Hour");
		}
		else{
			System.out.println("I just dived at 1000 km's per Hour");
		}
	}
	
	public void eat(String name){
		System.out.println(name + " likes to eat fishes");
	}
}